# Import socket module
import socket
import const
from struct import pack, unpack
 
 
def Main():
    # local host IP '127.0.0.1'
    host = '127.0.0.1'
 
    # Define the port on which you want to connect
    port = 12345
 
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
 
    # connect to server on local computer
    s.connect((host,port))
    def make_commkey(key, session_id, ticks=50):
        """
        take a password and session_id and scramble them to send to the machine.
        copied from commpro.c - MakeKey
        """
        key = int(key)
        session_id = int(session_id)
        k = 0
        for i in range(32):
            if (key & (1 << i)):
                k = (k << 1 | 1)
            else:
                k = k << 1
        k += session_id

        k = pack(b'I', k)
        k = unpack(b'BBBB', k)
        k = pack(
            b'BBBB',
            k[0] ^ ord('Z'),
            k[1] ^ ord('K'),
            k[2] ^ ord('S'),
            k[3] ^ ord('O'))
        k = unpack(b'HH', k)
        k = pack(b'HH', k[1], k[0])

        B = 0xff & ticks
        k = unpack(b'BBBB', k)
        k = pack(
            b'BBBB',
            k[0] ^ B,
            k[1] ^ B,
            B,
            k[3] ^ B)
        return k
 
    # message you send to server
    # message = const.USHRT_MAX
    
    
    # message ="ZKSO"
    message = make_commkey(1500, 100, 50)
    while True:
 
        # message sent to server
        s.send(message)
        # s.send(message.)
        # s.send(int(bytes(message.encode('ascii'))))
 
        # message received from server
        data = s.recv(1024)
        # datax = s.recv()
 
        # print the received message
        # here it would be a reverse of sent message
        # print('Received from the server :',str(data.decode('ascii')))
        print('Received from the server :',(data))
 
        # ask the client whether he wants to continue
        ans = input('\nDo you want to continue(y/n) :')
        if ans == 'y':
            continue
        else:
            break
    # close the connection
    s.close()
 
    

 
if __name__ == '__main__':
    Main()
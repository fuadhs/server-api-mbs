from app.repository.blok_repository import BlokRepository
from app.schema.blok_schema import BaseBlok,FindBlokWithKode, FindKebunWithSap, UpsertBlok
from app.services.base_service import BaseService


class BlokService(BaseService):
    def __init__(self, blok_repository: BlokRepository):
        self.blok_repository = blok_repository
        super().__init__(blok_repository)

    def add(self, schema: BaseBlok):
        find_kode_blok = FindBlokWithKode()
        find_kode_blok.page_size = "all"
        return self.blok_repository.create_blok(schema)

    def patch(self, id: int, schema: BaseBlok):
        find_kode_blok = FindBlokWithKode()
        find_kode_blok.page_size = "all"
        return self.blok_repository.update_blok(id, schema)



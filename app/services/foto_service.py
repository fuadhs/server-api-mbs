from app.repository.foto_repository import FotoRepository
from app.schema.foto_schema import BaseFoto,FindFotoNama, FindFotoResult, UpsertFoto
from app.services.base_service import BaseService


class FotoService(BaseService):
    def __init__(self, foto_repository: FotoRepository):
        self.foto_repository = foto_repository
        super().__init__(foto_repository)

    def add(self, schema: BaseFoto):
        find_nama = FindFotoNama()
        find_nama.page_size = "all"
        return self.foto_repository.create_foto(schema)

    def patch(self, id: int, schema: BaseFoto):
        find_nama = FindFotoNama()
        find_nama.page_size = "all"
        return self.foto_repository.update_foto(id, schema)



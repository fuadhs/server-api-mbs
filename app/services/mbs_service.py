from app.repository.mbs_repository import MbsRepository
from app.schema.mbs_schema import BaseMbs,FindMbsWithKodeSpta, FindMbsWithNopol,FindMbsResult, UpsertMbs
from app.services.base_service import BaseService


class MbsService(BaseService):
    def __init__(self, mbs_repository: MbsRepository):
        self.mbs_repository = mbs_repository
        super().__init__(mbs_repository)

    def add(self, schema: BaseMbs):
        find_spta = FindMbsWithKodeSpta()
        find_spta.page_size = "all"
        return self.mbs_repository.create_mbs(schema)

    def patch(self, id: int, schema: BaseMbs):
        find_spta = FindMbsWithKodeSpta()
        find_spta.page_size = "all"
        return self.mbs_repository.update_mbs(id, schema)



from app.repository.faktormbs_repository import FaktorMbsRepository
from app.schema.faktormbs_schema import BaseFaktorMbs,FindFaktorMbsWithMutu, FindFaktorMbsResult, UpsertFaktormbs
from app.services.base_service import BaseService


class FaktorMbsService(BaseService):
    def __init__(self, faktormbs_repository: FaktorMbsRepository):
        self.faktormbs_repository = faktormbs_repository
        super().__init__(faktormbs_repository)

    def add(self, schema: BaseFaktorMbs):
        find_mutu = FindFaktorMbsWithMutu()
        find_mutu.page_size = "all"
        return self.faktormbs_repository.create_faktormbs(schema)

    def patch(self, id: int, schema: BaseFaktorMbs):
        find_mutu = FindFaktorMbsWithMutu()
        find_mutu.page_size = "all"
        return self.faktormbs_repository.update_faktormbs(id, schema)



from app.repository.afdeling_repository import AfdelingRepository
from app.schema.afdeling_schema import BaseAfdeling,FindAfdelingWithKode, FindAfdelingWithNama, UpsertAfdeling
from app.services.base_service import BaseService


class AfdelingService(BaseService):
    def __init__(self, afdeling_repository: AfdelingRepository):
        self.afdeling_repository = afdeling_repository
        super().__init__(afdeling_repository)

    def add(self, schema: BaseAfdeling):
        find_nama_afdeling = FindAfdelingWithKode()
        find_nama_afdeling.page_size = "all"
        return self.afdeling_repository.create_afdeling(schema)

    def patch(self, id: int, schema: BaseAfdeling):
        find_nama_afdeling = FindAfdelingWithKode()
        find_nama_afdeling.page_size = "all"
        return self.afdeling_repository.update_afdeling(id, schema)



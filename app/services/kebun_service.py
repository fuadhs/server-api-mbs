from app.repository.kebun_repository import KebunRepository
from app.schema.kebun_schema import BaseKebun,FindKebunWithKode, FindKebunWithNama, UpsertKebun
from app.services.base_service import BaseService


class KebunService(BaseService):
    def __init__(self, kebun_repository: KebunRepository):
        self.kebun_repository = kebun_repository
        super().__init__(kebun_repository)

    def add(self, schema: BaseKebun):
        find_nama_kebun = FindKebunWithNama()
        find_nama_kebun.page_size = "all"
        return self.kebun_repository.create_kebun(schema)

    def patch(self, id: int, schema: BaseKebun):
        find_nama_kebun = FindKebunWithNama()
        find_nama_kebun.page_size = "all"
        return self.kebun_repository.update_kebun(id, schema)



from app.repository.pemilik_repository import PemilikRepository
from app.schema.pemilik_schema import BasePemilik,FindPemilik, UpsertPemilik
from app.services.base_service import BaseService


class PemilikService(BaseService):
    def __init__(self, pemilik_repository: PemilikRepository):
        self.pemilik_repository = pemilik_repository
        # self.tag_repository = tag_repository
        super().__init__(pemilik_repository)

    def add(self, schema: BasePemilik):
        find_nama = FindPemilik()
        find_nama.page_size = "all"
        # tags = None
        # if len(schema.tag_ids):
        #     find_tag.id__in = ",".join(map(str, schema.tag_ids))
        #     tags = self.tag_repository.read_by_options(find_tag)["founds"]
        # delattr(schema, "tag_ids")
        return self.pemilik_repository.create_pemilik(schema)

    def patch(self, id: int, schema: BasePemilik):
        find_nama = FindPemilik()
        find_nama.page_size = "all"
        # tags = None
        # if schema.tag_ids:
        #     find_tag.id__in = ",".join(map(str, schema.tag_ids))
        #     tags = self.tag_repository.read_by_options(find_tag)["founds"]
        # delattr(schema, "tag_ids")
        return self.pemilik_repository.update_pemilik(id, schema)



# from app.repository.pemilik_repository import PemilikRepository
# from app.services.base_service import BaseService


# class PemilikService(BaseService):
#     def __init__(self, pemilik_repository: PemilikRepository):
#         self.pemilik_repository = pemilik_repository
#         super().__init__(pemilik_repository)

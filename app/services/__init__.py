from app.services.auth_service import AuthService
from app.services.post_service import PostService
from app.services.tag_service import TagService
from app.services.user_service import UserService

from app.services.pemilik_service import PemilikService
from app.services.kebun_service import KebunService
from app.services.afdeling_service import AfdelingService
from app.services.blok_service import BlokService
from app.services.faktormbs_service import FaktorMbsService
from app.services.mbs_service import MbsService
from app.services.foto_service import FotoService
from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends

from app.core.container import Container
from app.core.dependencies import get_current_active_user
from app.model.user import User
from app.schema.base_schema import Blank
from app.schema.kebun_schema import FindKebunWithKode, FindKebunResult, UpsertKebun, BaseKebun
from app.services.kebun_service import KebunService

router = APIRouter(
    prefix="/kebun",
    tags=["kebun"],
)


@router.get("", response_model=FindKebunResult)
@inject
async def get_kebun_list(
    find_query: FindKebunWithKode = Depends(),
    service: KebunService = Depends(Provide[Container.kebun_service]),
):
    return service.get_list(find_query)


# @router.get("/{post_id}", response_model=PostWithTags)
# @inject
# async def get_post(
#     post_id: int,
#     service: PostService = Depends(Provide[Container.post_service]),
# ):
#     return service.get_by_id(post_id)


@router.post("", response_model=BaseKebun)
@inject
async def create_post(
    kebun: BaseKebun,
    service: KebunService = Depends(Provide[Container.kebun_service]),
    current_user: User = Depends(get_current_active_user),
):
    kebun.user_token = current_user.user_token
    return service.add(kebun)


@router.patch("/{kebun_id}", response_model=BaseKebun)
@inject
async def update_post(
    kebun_id: int,
    post: UpsertKebun,
    service: KebunService = Depends(Provide[Container.kebun_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.patch(kebun_id, post)


@router.delete("/{kebun_id}", response_model=Blank)
@inject
async def delete_post(
    kebun_id: int,
    service: KebunService = Depends(Provide[Container.kebun_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.remove_by_id(kebun_id)

from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends, File, UploadFile

from app.core.container import Container
from app.core.dependencies import get_current_active_user
from app.model.user import User
from app.schema.base_schema import Blank
from app.schema.foto_schema import BaseFoto, FindFotoNama, FindFotoResult , UpsertFoto
from app.services.foto_service import FotoService

router = APIRouter(
    prefix="/foto",
    tags=["foto"],
)


@router.get("", response_model=FindFotoResult)
@inject
async def get_foto_list(
    find_query: FindFotoNama = Depends(),
    service: FotoService = Depends(Provide[Container.foto_service]),
):
    return service.get_list(find_query)


# @router.get("/{post_id}", response_model=PostWithTags)
# @inject
# async def get_post(
#     post_id: int,
#     service: PostService = Depends(Provide[Container.post_service]),
# ):
#     return service.get_by_id(post_id)


@router.post("", response_model=BaseFoto)
@inject
async def create_post(
    foto: BaseFoto,
    service: FotoService = Depends(Provide[Container.foto_service]),
    current_user: User = Depends(get_current_active_user),
):
    foto.user_token = current_user.user_token
    return service.add(foto)

@router.patch("/{foto_id}", response_model=BaseFoto)
@inject
async def update_blok(
    foto_id: int,
    post: UpsertFoto,
    service: FotoService = Depends(Provide[Container.foto_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.patch(foto_id, post)


@router.delete("/{foto_id}", response_model=Blank)
@inject
async def delete_post(
    foto_id: int,
    service: FotoService = Depends(Provide[Container.foto_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.remove_by_id(foto_id)

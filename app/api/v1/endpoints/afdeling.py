from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends

from app.core.container import Container
from app.core.dependencies import get_current_active_user
from app.model.user import User
from app.schema.base_schema import Blank
from app.schema.afdeling_schema import FindAfdelingWithKode, FindAfdelingResult, FindAfdelingWithNama, UpsertAfdeling, BaseAfdeling
from app.services.afdeling_service import AfdelingService

router = APIRouter(
    prefix="/afdeling",
    tags=["afdeling"],
)


@router.get("", response_model=FindAfdelingResult)
@inject
async def get_afdeling_list(
    find_query: FindAfdelingWithKode = Depends(),
    service: AfdelingService = Depends(Provide[Container.afdeling_service]),
):
    return service.get_list(find_query)


# @router.get("/{post_id}", response_model=PostWithTags)
# @inject
# async def get_post(
#     post_id: int,
#     service: PostService = Depends(Provide[Container.post_service]),
# ):
#     return service.get_by_id(post_id)


@router.post("", response_model=BaseAfdeling)
@inject
async def create_post(
    afdeling: BaseAfdeling,
    service: AfdelingService = Depends(Provide[Container.afdeling_service]),
    current_user: User = Depends(get_current_active_user),
):
    afdeling.user_token = current_user.user_token
    return service.add(afdeling)


@router.patch("/{afdeling_id}", response_model=BaseAfdeling)
@inject
async def update_afdeling(
    afdeling_id: int,
    post: UpsertAfdeling,
    service: AfdelingService = Depends(Provide[Container.afdeling_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.patch(afdeling_id, post)


@router.delete("/{afdeling_id}", response_model=Blank)
@inject
async def delete_post(
    afdeling_id: int,
    service: AfdelingService = Depends(Provide[Container.afdeling_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.remove_by_id(afdeling_id)

from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends

from app.core.container import Container
from app.core.dependencies import get_current_active_user
from app.model.user import User
from app.schema.base_schema import Blank
from app.schema.pemilik_schema import FindPemilik, FindPemilikResult, UpsertPemilik, BasePemilik
from app.services.pemilik_service import PemilikService

router = APIRouter(
    prefix="/pemilik",
    tags=["pemilik"],
)


@router.get("", response_model=FindPemilikResult)
@inject
async def get_post_list(
    find_query: FindPemilik = Depends(),
    service: PemilikService = Depends(Provide[Container.pemilik_service]),
):
    return service.get_list(find_query)


# @router.get("/{post_id}", response_model=PostWithTags)
# @inject
# async def get_post(
#     post_id: int,
#     service: PostService = Depends(Provide[Container.post_service]),
# ):
#     return service.get_by_id(post_id)


@router.post("", response_model=BasePemilik)
@inject
async def create_pemilik(
    pemiliks: BasePemilik,
    service: PemilikService = Depends(Provide[Container.pemilik_service]),
    current_user: User = Depends(get_current_active_user),
):
    pemiliks.user_token = current_user.user_token
    return service.add(pemiliks)


@router.patch("/{pemilik_id}", response_model=FindPemilikResult)
@inject
async def update_pemilik(
    pemilik_id: int,
    pemilik: UpsertPemilik,
    service: PemilikService = Depends(Provide[Container.pemilik_service]),
    current_user: User = Depends(get_current_active_user),
):
    pemilik.user_token = current_user.user_token
    return service.patch(pemilik_id, pemilik)


@router.delete("/{pemilik_id}", response_model=Blank)
@inject
async def delete_pemilik(
    pemilik_id: int,
    # pemilik: UpsertPemilik,
    service: PemilikService = Depends(Provide[Container.pemilik_service]),
    current_user: User = Depends(get_current_active_user),
):
    # pemilik.user_token = current_user.user_token    
    return service.remove_by_id(pemilik_id)

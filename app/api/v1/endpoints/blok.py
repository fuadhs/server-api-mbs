from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends

from app.core.container import Container
from app.core.dependencies import get_current_active_user
from app.model.user import User
from app.schema.base_schema import Blank
from app.schema.blok_schema import BaseBlok, FindBlokResult, FindBlokWithKode, FindKebunWithSap , UpsertBlok
from app.services.blok_service import BlokService

router = APIRouter(
    prefix="/blok",
    tags=["blok"],
)


@router.get("", response_model=FindBlokResult)
@inject
async def get_blok_list(
    find_query: FindBlokWithKode = Depends(),
    service: BlokService = Depends(Provide[Container.blok_service]),
):
    return service.get_list(find_query)


# @router.get("/{post_id}", response_model=PostWithTags)
# @inject
# async def get_post(
#     post_id: int,
#     service: PostService = Depends(Provide[Container.post_service]),
# ):
#     return service.get_by_id(post_id)


@router.post("", response_model=BaseBlok)
@inject
async def create_post(
    blok: BaseBlok,
    service: BlokService = Depends(Provide[Container.blok_service]),
    current_user: User = Depends(get_current_active_user),
):
    blok.user_token = current_user.user_token
    return service.add(blok)


@router.patch("/{blok_id}", response_model=BaseBlok)
@inject
async def update_blok(
    blok_id: int,
    post: UpsertBlok,
    service: BlokService = Depends(Provide[Container.blok_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.patch(blok_id, post)


@router.delete("/{blok_id}", response_model=Blank)
@inject
async def delete_post(
    blok_id: int,
    service: BlokService = Depends(Provide[Container.blok_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.remove_by_id(blok_id)

from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends

from app.core.container import Container
from app.core.dependencies import get_current_active_user
from app.model.user import User
from app.schema.base_schema import Blank
from app.schema.mbs_schema import BaseMbs, FindMbsWithKodeSpta, FindMbsWithNopol, FindMbsResult , UpsertMbs
from app.services.mbs_service import MbsService

router = APIRouter(
    prefix="/mbs",
    tags=["mbs"],
)


@router.get("", response_model=FindMbsResult)
@inject
async def get_mbs_list(
    find_query: FindMbsWithKodeSpta = Depends(),
    service: MbsService = Depends(Provide[Container.mbs_service]),
):
    return service.get_list(find_query)


# @router.get("/{post_id}", response_model=PostWithTags)
# @inject
# async def get_post(
#     post_id: int,
#     service: PostService = Depends(Provide[Container.post_service]),
# ):
#     return service.get_by_id(post_id)


@router.post("", response_model=BaseMbs)
@inject
async def create_mbs(
    mbs: BaseMbs,
    service: MbsService = Depends(Provide[Container.mbs_service]),
    current_user: User = Depends(get_current_active_user),
):
    mbs.user_token = current_user.user_token
    return service.add(mbs)

@router.patch("/{mbs_id}", response_model=BaseMbs)
@inject
async def update_mbs(
    mbs_id: int,
    post: UpsertMbs,
    service: MbsService = Depends(Provide[Container.mbs_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.patch(mbs_id, post)


@router.delete("/{mbs_id}", response_model=Blank)
@inject
async def delete_post(
    mbs_id: int,
    service: MbsService = Depends(Provide[Container.mbs_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.remove_by_id(mbs_id)

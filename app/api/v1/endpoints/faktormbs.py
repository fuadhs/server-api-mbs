from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends

from app.core.container import Container
from app.core.dependencies import get_current_active_user
from app.model.user import User
from app.schema.base_schema import Blank
from app.schema.faktormbs_schema import BaseFaktorMbs, FindFaktorMbsWithMutu, FindFaktorMbsResult , UpsertFaktormbs
from app.services.faktormbs_service import FaktorMbsService

router = APIRouter(
    prefix="/faktormbs",
    tags=["faktormbs"],
)


@router.get("", response_model=FindFaktorMbsResult)
@inject
async def get_faktormbs_list(
    find_query: FindFaktorMbsWithMutu = Depends(),
    service: FaktorMbsService = Depends(Provide[Container.faktormbs_service]),
):
    return service.get_list(find_query)


# @router.get("/{post_id}", response_model=PostWithTags)
# @inject
# async def get_post(
#     post_id: int,
#     service: PostService = Depends(Provide[Container.post_service]),
# ):
#     return service.get_by_id(post_id)


@router.post("", response_model=BaseFaktorMbs)
@inject
async def create_post(
    faktormbs: BaseFaktorMbs,
    service: FaktorMbsService = Depends(Provide[Container.faktormbs_service]),
    current_user: User = Depends(get_current_active_user),
):
    faktormbs.user_token = current_user.user_token
    return service.add(faktormbs)

@router.patch("/{faktormbs_id}", response_model=BaseFaktorMbs)
@inject
async def update_blok(
    faktormbs_id: int,
    post: UpsertFaktormbs,
    service: FaktorMbsService = Depends(Provide[Container.faktormbs_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.patch(faktormbs_id, post)


@router.delete("/{faktormbs_id}", response_model=Blank)
@inject
async def delete_post(
    faktormbs_id: int,
    service: FaktorMbsService = Depends(Provide[Container.faktormbs_service]),
    current_user: User = Depends(get_current_active_user),
):
    return service.remove_by_id(faktormbs_id)

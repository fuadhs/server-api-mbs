from fastapi import APIRouter

from app.api.v1.endpoints.auth import router as auth_router
from app.api.v1.endpoints.post import router as post_router
from app.api.v1.endpoints.tag import router as tag_router
from app.api.v1.endpoints.user import router as user_router
from app.api.v1.endpoints.pemilik import router as pemilik_router
from app.api.v1.endpoints.kebun import router as kebun_router
from app.api.v1.endpoints.afdeling import router as afdeling_router
from app.api.v1.endpoints.blok import router as blok_router
from app.api.v1.endpoints.faktormbs import router as faktormbs_router
from app.api.v1.endpoints.mbs import router as mbs_router
from app.api.v1.endpoints.foto import router as foto_router

routers = APIRouter()
router_list = [
    auth_router, 
    post_router, 
    tag_router, 
    user_router, 
    pemilik_router, 
    kebun_router,
    afdeling_router,
    blok_router,
    faktormbs_router,
    mbs_router,
    foto_router]

for router in router_list:
    router.tags = routers.tags.append("v1")
    routers.include_router(router)

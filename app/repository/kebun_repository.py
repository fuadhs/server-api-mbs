from contextlib import AbstractContextManager
from typing import Callable

from sqlalchemy.orm import Session

from app.model.kebun import Kebun
from app.repository.base_repository import BaseRepository
from app.schema.kebun_schema import BaseKebun


class KebunRepository(BaseRepository):
    def __init__(self, session_factory: Callable[..., AbstractContextManager[Session]]):
        super().__init__(session_factory, Kebun)

    def create_kebun(self, schema: BaseKebun):
        with self.session_factory() as session:
            query = self.model(**schema.dict())
            session.add(query)
            session.commit()
            session.refresh(query)
            return query

    def update_kebun(self, id: int, schema: BaseKebun):
        with self.session_factory() as session:
            session.query(self.model).filter(self.model.id == id).update(schema.dict(exclude_none=True))
            query = session.query(self.model).filter(self.model.id == id).first()
            session.commit()
            session.refresh(query)
            return self.read_by_id(id)

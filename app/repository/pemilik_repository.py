from contextlib import AbstractContextManager
from typing import Callable

from sqlalchemy.orm import Session

from app.model.pemilik import Pemilik
from app.repository.base_repository import BaseRepository
from app.schema.pemilik_schema import BasePemilik


class PemilikRepository(BaseRepository):
    def __init__(self, session_factory: Callable[..., AbstractContextManager[Session]]):
        super().__init__(session_factory, Pemilik)

    def create_pemilik(self, schema: BasePemilik):
        with self.session_factory() as session:
            query = self.model(**schema.dict())
            session.add(query)
            session.commit()
            session.refresh(query)
            return query

    def update_pemilik(self, id: int, schema: BasePemilik):
        with self.session_factory() as session:
            session.query(self.model).filter(self.model.id == id).update(schema.dict(exclude_none=True))
            query = session.query(self.model).filter(self.model.id == id).first()
            # if tags:
            #     query.tags = []
            #     session.flush()
            #     query.tags = tags
            # else:
            #     query.tags = []
            session.commit()
            session.refresh(query)
            return self.read_by_id(id)

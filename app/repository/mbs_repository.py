from contextlib import AbstractContextManager
from typing import Callable

from sqlalchemy.orm import Session

from app.model.mbs import MBS
from app.repository.base_repository import BaseRepository
from app.schema.mbs_schema import BaseMbs


class MbsRepository(BaseRepository):
    def __init__(self, session_factory: Callable[..., AbstractContextManager[Session]]):
        super().__init__(session_factory, MBS)

    def create_mbs(self, schema: BaseMbs):
        with self.session_factory() as session:
            query = self.model(**schema.dict())
            session.add(query)
            session.commit()
            session.refresh(query)
            return query

    def update_mbs(self, id: int, schema: BaseMbs):
        with self.session_factory() as session:
            session.query(self.model).filter(self.model.id == id).update(schema.dict(exclude_none=True))
            query = session.query(self.model).filter(self.model.id == id).first()
            session.commit()
            session.refresh(query)
            return self.read_by_id(id)

from typing import List, Optional

from pydantic import BaseModel

from app.schema.base_schema import FindBase, ModelBaseInfo, SearchOptions
from app.util.schema import AllOptional


class BaseMbs(BaseModel):
    user_token: str
    kode_spta: str
    nopol:str
    mutu: str
    keterangan: str
    afdeling_id: str
    foto_id: str
    
    class Config:
        orm_mode = True


class Mbs(ModelBaseInfo, BaseMbs, metaclass=AllOptional):
    ...

class FindMbsWithNopol(FindBase, BaseMbs, metaclass=AllOptional):
    nopol__eq: str
    ...
    
class FindMbsWithKodeSpta(FindBase, BaseMbs, metaclass=AllOptional):
    kode_spta__eq: str
    ...


class UpsertMbs(BaseMbs, metaclass=AllOptional):
    ...


class FindMbsResult(BaseModel):
    founds: Optional[List[Mbs]]
    search_options: Optional[SearchOptions]

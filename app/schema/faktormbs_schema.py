from typing import List, Optional

from pydantic import BaseModel

from app.schema.base_schema import FindBase, ModelBaseInfo, SearchOptions
from app.util.schema import AllOptional


class BaseFaktorMbs(BaseModel):
    user_token: str
    mutu: str
    keterangan: str

    
    class Config:
        orm_mode = True


class FaktorMbs(ModelBaseInfo, BaseFaktorMbs, metaclass=AllOptional):
    ...

class FindFaktorMbsWithMutu(FindBase, BaseFaktorMbs, metaclass=AllOptional):
    kode_mutu__eq: str
    ...

# class FindKebunWithSap(FindBase, BaseFaktorMbs, metaclass=AllOptional):
#     nama_sap__eq: str
#     ...


class UpsertFaktormbs(BaseFaktorMbs, metaclass=AllOptional):
    ...


class FindFaktorMbsResult(BaseModel):
    founds: Optional[List[FaktorMbs]]
    search_options: Optional[SearchOptions]

from typing import List, Optional

from pydantic import BaseModel

from app.schema.base_schema import FindBase, ModelBaseInfo, SearchOptions
from app.util.schema import AllOptional


class BaseKebun(BaseModel):
    user_token: str
    kode_kebun: str
    nama_kebun: str
    pemilik_id: str
    
    class Config:
        orm_mode = True


class Kebun(ModelBaseInfo, BaseKebun, metaclass=AllOptional):
    ...

class FindKebunWithKode(FindBase, BaseKebun, metaclass=AllOptional):
    kode_kebun__eq: str
    ...

class FindKebunWithNama(FindBase, BaseKebun, metaclass=AllOptional):
    nama_kebun__eq: str
    ...


class UpsertKebun(BaseKebun, metaclass=AllOptional):
    ...


class FindKebunResult(BaseModel):
    founds: Optional[List[Kebun]]
    search_options: Optional[SearchOptions]

from typing import List, Optional

from pydantic import BaseModel

from app.schema.base_schema import FindBase, ModelBaseInfo, SearchOptions
from app.util.schema import AllOptional


class BasePemilik(BaseModel):
    user_token: str
    nama_pemilik: str
    ktp: str
    hp: str
    alamat:str
    kategori:str
    wilayah: str

    class Config:
        orm_mode = True


# class BasePemilikWithNama(BasePemilik):
#     nama_pemilik: str


class Pemilik(ModelBaseInfo, BasePemilik, metaclass=AllOptional):
    ...


class FindPemilik(FindBase, BasePemilik, metaclass=AllOptional):
    nama_pemilik__eq: str
    ...


class UpsertPemilik(BasePemilik, metaclass=AllOptional):
    ...


class FindPemilikResult(BaseModel):
    founds: Optional[List[Pemilik]]
    search_options: Optional[SearchOptions]

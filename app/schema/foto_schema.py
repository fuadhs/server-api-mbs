from typing import List, Optional

from pydantic import BaseModel

from app.schema.base_schema import FindBase, ModelBaseInfo, SearchOptions
from app.util.schema import AllOptional


class BaseFoto(BaseModel):
    user_token: str
    lokasi: str
    nama: str

    
    class Config:
        orm_mode = True


class Foto(ModelBaseInfo, BaseFoto, metaclass=AllOptional):
    ...

class FindFotoNama(FindBase, BaseFoto, metaclass=AllOptional):
    nama__eq: str
    ...

# class FindKebunWithSap(FindBase, BaseFoto, metaclass=AllOptional):
#     nama_sap__eq: str
#     ...


class UpsertFoto(BaseFoto, metaclass=AllOptional):
    ...


class FindFotoResult(BaseModel):
    founds: Optional[List[Foto]]
    search_options: Optional[SearchOptions]

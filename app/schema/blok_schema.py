from typing import List, Optional

from pydantic import BaseModel

from app.schema.base_schema import FindBase, ModelBaseInfo, SearchOptions
from app.util.schema import AllOptional


class BaseBlok(BaseModel):
    user_token: str
    kode_blok: str
    kode_sap: str
    kategori:str
    afdeling_id: str
    kebun_id: str
    pemilik_id: str
    
    class Config:
        orm_mode = True


class Blok(ModelBaseInfo, BaseBlok, metaclass=AllOptional):
    ...

class FindBlokWithKode(FindBase, BaseBlok, metaclass=AllOptional):
    kode_kode__eq: str
    ...

class FindKebunWithSap(FindBase, BaseBlok, metaclass=AllOptional):
    nama_sap__eq: str
    ...


class UpsertBlok(BaseBlok, metaclass=AllOptional):
    ...


class FindBlokResult(BaseModel):
    founds: Optional[List[Blok]]
    search_options: Optional[SearchOptions]

from typing import List, Optional

from pydantic import BaseModel

from app.schema.base_schema import FindBase, ModelBaseInfo, SearchOptions
from app.util.schema import AllOptional


class BaseAfdeling(BaseModel):
    user_token: str
    kode_afdeling: str
    nama_afdeling: str
    kebun_id: str
    class Config:
        orm_mode = True


class Afdeling(ModelBaseInfo, BaseAfdeling, metaclass=AllOptional):
    ...

class FindAfdelingWithKode(FindBase, BaseAfdeling, metaclass=AllOptional):
    kode_afdeling__eq: str
    ...

class FindAfdelingWithNama(FindBase, BaseAfdeling, metaclass=AllOptional):
    nama_afdeling__eq: str
    ...


class UpsertAfdeling(BaseAfdeling, metaclass=AllOptional):
    ...


class FindAfdelingResult(BaseModel):
    founds: Optional[List[Afdeling]]
    search_options: Optional[SearchOptions]

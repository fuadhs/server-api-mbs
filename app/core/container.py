from dependency_injector import containers, providers

from app.core.config import configs
from app.core.database import Database
from app.repository import *
from app.services import *


class Container(containers.DeclarativeContainer):
    wiring_config = containers.WiringConfiguration(
        modules=[
            "app.api.v1.endpoints.auth",
            "app.api.v1.endpoints.post",
            "app.api.v1.endpoints.tag",
            "app.api.v1.endpoints.user",
            "app.api.v1.endpoints.pemilik",
            "app.api.v1.endpoints.kebun",
            "app.api.v1.endpoints.afdeling",
            "app.api.v1.endpoints.blok",
            "app.api.v1.endpoints.faktormbs",
            "app.api.v1.endpoints.mbs",
            "app.api.v1.endpoints.foto",
            "app.api.v2.endpoints.auth",
            "app.core.dependencies",
        ]
    )

    db = providers.Singleton(Database, db_url=configs.DATABASE_URI)

    post_repository = providers.Factory(PostRepository, session_factory=db.provided.session)
    tag_repository = providers.Factory(TagRepository, session_factory=db.provided.session)
    user_repository = providers.Factory(UserRepository, session_factory=db.provided.session)
    # Master
    pemilik_repository = providers.Factory(PemilikRepository, session_factory=db.provided.session)
    kebun_repository = providers.Factory(KebunRepository, session_factory=db.provided.session)
    afdeling_repository = providers.Factory(AfdelingRepository,session_factory=db.provided.session)
    blok_repository = providers.Factory(BlokRepository, session_factory=db.provided.session)
    faktormbs_repository=providers.Factory(FaktorMbsRepository,session_factory=db.provided.session)
    mbs_repository = providers.Factory(MbsRepository,session_factory=db.provided.session)
    foto_repository = providers.Factory(FotoRepository,session_factory=db.provided.session)
    
    auth_service = providers.Factory(AuthService, user_repository=user_repository)
    post_service = providers.Factory(PostService, post_repository=post_repository, tag_repository=tag_repository)
    tag_service = providers.Factory(TagService, tag_repository=tag_repository)
    user_service = providers.Factory(UserService, user_repository=user_repository)
    # Master
    pemilik_service = providers.Factory(PemilikService, pemilik_repository=pemilik_repository)
    kebun_service = providers.Factory(KebunService, kebun_repository=kebun_repository)
    afdeling_service = providers.Factory(AfdelingService, afdeling_repository=afdeling_repository)
    blok_service = providers.Factory(BlokService, blok_repository= blok_repository)
    faktormbs_service = providers.Factory(FaktorMbsService, faktormbs_repository=faktormbs_repository)
    mbs_service = providers.Factory(MbsService, mbs_repository=mbs_repository)    
    foto_service = providers.Factory(FotoService, foto_repository=foto_repository)    
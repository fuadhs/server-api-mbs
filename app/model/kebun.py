from sqlmodel import Field

from app.model.base_model import BaseModel


class Kebun(BaseModel, table=True):
    __tablename__ = "kebun"
    user_token: str = Field()
    kode_kebun: str = Field(default=None, nullable=False, index=True)
    nama_kebun: str = Field(default=None, nullable=False, index=True)
    pemilik_id: int = Field(foreign_key="pemilik.id", index=True,nullable=False)
    
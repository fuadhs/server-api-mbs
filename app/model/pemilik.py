from sqlmodel import Field

from app.model.base_model import BaseModel


class Pemilik(BaseModel, table=True):
    __tablename__ = "pemilik"
    user_token: str = Field()
    nama_pemilik: str = Field(default=None, nullable=False, index=True)
    ktp: str = Field(default=None, nullable=True, unique=True)
    hp: str = Field(default=None, nullable=True)
    alamat: str = Field(default=None, nullable=True)
    kategori: str = Field(default=None, nullable=False)
    wilayah: str = Field(default=None, nullable=False)
    # is_published: bool = Field(default=False)

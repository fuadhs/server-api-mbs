from sqlmodel import Field

from app.model.base_model import BaseModel


class Foto(BaseModel, table=True):
    __tablename__ = "foto"
    user_token: str = Field()
    lokasi: str = Field(nullable=True, index=True)
    nama: str = Field(nullable=True, index=True)
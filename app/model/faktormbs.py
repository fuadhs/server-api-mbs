from sqlmodel import Field

from app.model.base_model import BaseModel


class FaktorMbs(BaseModel, table=True):
    __tablename__ = "faktormbs"
    user_token: str = Field()
    mutu: str = Field(default=None, nullable=False, index=True, unique=True)
    keterangan: str = Field(default=None, nullable=False, index=True)
    
    

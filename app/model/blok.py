from sqlmodel import Field

from app.model.base_model import BaseModel


class Blok(BaseModel, table=True):
    __tablename__ = "blok"
    user_token: str = Field()
    kode_blok: str = Field(default=None, nullable=False, index=True, unique=True)
    kode_sap: str = Field(default=None, nullable=False, index=True)
    kategori: str = Field(default=None, nullable=False, index=True)
    afdeling_id: int = Field(foreign_key="afdeling.id",index=True,nullable=False)
    kebun_id: int = Field(foreign_key="kebun.id",index=True,nullable=False)
    pemilik_id: int = Field(foreign_key="pemilik.id",index=True,nullable=False)
    # post_id: int = Field(foreign_key="post.id", primary_key=True, index=True, nullable=False)
    

from sqlmodel import Field

from app.model.base_model import BaseModel


class MBS(BaseModel, table=True):
    __tablename__ = "mbs"
    user_token: str = Field()
    kode_spta: str = Field(default=None, nullable=False, index=True, unique=True)
    nopol: str = Field(default=None, nullable=False, index=True)
    mutu: str = Field(default=None, nullable=False, index=True)
    keterangan: str = Field(default=None, nullable=True, index=True)
    afdeling_id: int = Field(foreign_key="afdeling.id",index=True,nullable=False)
    foto_id:int= Field(foreign_key="foto.id", index=True, nullable=True)
    # kebun_id: int = Field(foreign_key="kebun.id",index=True,nullable=False)
    # pemilik_id: int = Field(foreign_key="pemilik.id",index=True,nullable=False)
    # post_id: int = Field(foreign_key="post.id", primary_key=True, index=True, nullable=False)
    

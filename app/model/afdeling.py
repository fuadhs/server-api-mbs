from sqlmodel import Field

from app.model.base_model import BaseModel


class Afdeling(BaseModel, table=True):
    __tablename__ = "afdeling"
    user_token: str = Field()
    kode_afdeling: str = Field(default=None, nullable=False, index=True, unique=True)
    nama_afdeling: str = Field(default=None, nullable=False, index=True)
    kebun_id: int = Field(foreign_key="kebun.id", index=True,nullable=False)
    # post_id: int = Field(foreign_key="post.id", primary_key=True, index=True, nullable=False)
    
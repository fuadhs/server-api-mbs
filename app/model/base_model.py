from datetime import datetime
# from typing import Optional

from sqlmodel import Column, DateTime, Field, SQLModel, func, BigInteger


class BaseModel(SQLModel):
    id: int = Field(primary_key=True, index=True,unique=True)
    created_at: datetime = Field(sa_column=Column(DateTime(timezone=True), default=func.now()))
    updated_at: datetime = Field(sa_column=Column(DateTime(timezone=True), default=func.now(), onupdate=func.now()))
    